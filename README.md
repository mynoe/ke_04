# ke_04
基于vue的网站示例
## Project setup 本地启动项目
1.安装node环境，确保电脑上安装了nodejs
2.进入项目根目录执行 npm install 安装项目依赖 如果安装了yarn包管理器 则 yarn install
  会装的快一些
3.依赖安装完成后，执行npm run start 如果安装了yarn包管理器 则 yarn start

### 打包 发布
1.开发完成后 执行 npm run build 如果安装了yarn包管理器 则 yarn build
2.生成的静态文件会发在 build文件夹下
3.将静态文件发到服务器根目录即可完成发布

### 访问地址
http://college_ad_web.jiuhenbang.com/
