var path = require('path')
function resolve(dir) {
  return path.join(__dirname, './', dir)
}
module.exports = {
  runtimeCompiler: true,
  configureWebpack: {
    module: {
      rules: [
        {
          test: /\.vue$/,
          use: [
            {
              loader: 'iview-loader',
              options: {
                prefix: true
              }
            }
          ]
        }
      ]
    },
  },
  devServer: {
    port:8088,
    proxy: {
      '': {
        target: 'http://college_ad.jiuhenbang.com/',
        changeOrigin: true,
        pathRewrite: {
          '^/api': ''
        }
      }
    }
  }
}