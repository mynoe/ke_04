import Vue from "vue";
import App from "./App.vue";
import VueRouter from 'vue-router'
// import router from "./router";
import defaultRoutes from './router'
import store from "./store";
import ViewUI from "view-design";

import api from './http/api/'
// import style
import "view-design/dist/styles/iview.css";

// import "./registerServiceWorker";

Vue.prototype.$api = api
Vue.config.productionTip = false;
Vue.use(ViewUI);
Vue.use(VueRouter)
// Vue.use(vshare)
window.GLOBAL_CONFIG = {}
GLOBAL_CONFIG.imgHost = 'http://college.jiuhenbang.com'

// if (router) {
//   console.log('router.component',router)
// }
Vue.component('DetaultComponent',{
  props:['name'],
  template:`<div v-html="content" style="height:100%;overflow: auto;"></div>`,
  data(){
    return {
      content:`<p style=""></p>`
    }
  },
  created() {
    console.log(this.$route)
    api.getCodeContent(`open-html-${this.$route.path.replace('/','')}`).then(({data})=>{
      if(data.code == 1){
        this.content = data.data[0].page_content
        var styleSheet = document.createElement('style');
          // script.type="text/javascript";
          styleSheet.innerText = data.data[0].css_style
          document.body.appendChild(styleSheet);
          var script = document.createElement('script');
          // script.type="text/javascript";
          script.innerText = data.data[0].js_style
          document.body.appendChild(script);
      }
    })
  },
  methods: {
    
  },
});
api.getStaticHtml().then(({data})=>{
  if(data.code == 1){
    data.data.forEach(item=>{
      defaultRoutes.push({
        path:`/${item.ad_code.replace('open-html-','')}`,
        name:`${item.ad_code.replace('open-html-','').toUpperCase()}`
      })
    })
    console.log('defaultRoutes',defaultRoutes)
    // commit('setPages',routes)
    
    // state.pages = routes
    
    defaultRoutes.forEach(router => {
      if (router.component) {
        // console.log('router.component',router)
        const component = router.component
        // console.log('-----','../src/views/' + router.name + '.vue')
        router.component = () => import(`./views/${router.name }.vue`)
        // router.component = resolve => {
          // 动态加载组件会编译加载项目所有组件
          // 这里不能全写变量，写开头确定起始地址，写结尾确定文件名
          // 这样就相当于编译'src/**/*.vue'，编译之后模块列表才会有所有的模块，传模块路径匹配才会命中
          
          // require([`../src/views/${router.name}.vue`], resolve)
        // }
      } else if (router.template) {
        router.component = resolve => {
          resolve({
            template: router.template
          })
        }
      } else {
        console.log('router',router)
        const component = `${router.name}/index`
        console.log('component',component)
        if(!router.name){
          console.log('router.redirect',router.redirect)
          router.component = () => import(`./views/forum/index.vue`)
        }else{
          console.log('router---',router.name)
          router.component = async resolve => {
            resolve({
              // template: `<div>${router.name}</div>`
              template: `<DetaultComponent name="${router.name}"/> `
            })
          }
        }
        // router.component = <DefaultTemplate/>
        // router.component = async resolve => {
          // 加载失败，不存在此模块，尝试加载本项目的视图
          // try {
          //   console.log('res---')
          //   // 尝试加载模块
          //   await require(['src/views/' + router.name + '.vue'], resolve)
          //   // router.component = () => import(`../views/${router.name }.vue`)
          // } catch {
          //   // 加载失败，不存在此模块，使用默认模板
          //   console.log(
          //     '(@|../..)/views/' + component + '.vue不存在，加载默认模板'
          //   )
            // resolve({
              // template: `<div>${router.name}</div>`
              // template: `<DetaultComponent name="${router.name}"/> `
            // })
          // }
        // }
      }
  
      let children = router.children
      if (children && children instanceof Array) {
        children = formatRoutes(children)
      }
  
      router.children = children
  
      // fmRoutes.push(router)
  
      // addRouters.push(r)
      // return fmRoutes
    })
    let router = new VueRouter({
      mode: 'hash',
      base: process.env.BASE_URL,
      routes: defaultRoutes
    })
    new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount("#app");
  }else if(data.code == 99){
    
    let router = new VueRouter({
      mode: 'hash',
      base: process.env.BASE_URL,
      routes: defaultRoutes
    })
    new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount("#app");
    store.commit('setShowLogin',true)
  }
});
// store.dispatch('getAppRoutes').then((res)=>{
  // console.log('store.state.res----',res)
  // new Vue({
  //   router,
  //   store,
  //   render: h => h(App)
  // }).$mount("#app");
// })


if(localStorage.getItem('userId')){
  store.dispatch('getUserInfo',{id:localStorage.getItem('userId')})
}
window.Vue = Vue || {}

window.addEventListener('hashchange',function(){
  console.log('hashchange');
  location.reload()
})