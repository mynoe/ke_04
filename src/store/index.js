import Vue from "vue";
import Vuex from "vuex";
import api from '../http/api/index'
import course from './modules/course'
import inspiration from './modules/inspiration'
import sucai from './modules/sucai'
import mine from './modules/mine'
import search from './modules/search'

import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

import Course from '../views/course/course.vue'
import CourseDetail from '../views/course/detail.vue'

//活动
// 社区
import Forum from '../views/Forum.vue'
// ui 部落
import UiBuluo from '../views/uiBuluo.vue'

import PassReset from '../views/PassReset.vue'

// 专题
import Java from '../views/Java.vue'
import Skill from '../views/Skill.vue'
import Android from '../views/Android.vue'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    userInfo:{},
    showLogin:false,
    pages:[
      {
        path:'',
        redirect:'/forum'
      },
      {
        path: '/login',
        name: 'Login',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import('../views/Login.vue'),
      },
      {
        path: '/regist',
        name: 'Regist',
        component: () => import('../views/Regist.vue'),
      },
      {
        path:'/passreset',
        name:'PassReset',
        component:PassReset
      },
      {
        path: '/home',
        name: 'Home',
        component: Home,
      },
      {
        path: '/course',
        name: 'Course',
        component: Course
      },
      {
        path: '/courseDetail',
        name: 'CourseDetail',
        component: CourseDetail
      },
      {
        path:'/uibuluo',
        name:'UiBuluo',
        component:UiBuluo
      },
      {
        path:'/java',
        name:'Java',
        component:Java
      },
      {
        path:'/skill',
        name:'Skill',
        component:Skill
      },
      {
        path:'/android',
        name:'Android',
        component:Android
      },
      {
        path:'/forum',
        name:'Forum',
        component:Forum
      },
    ]
  },
  mutations: {
    setUserInfo(state,payload){
      state.userInfo = payload
    },
    setShowLogin(state,payload){
      console.log('payload',payload)
      state.showLogin = payload
    },
    setPages(state,payload){
      state.pages = payload
    }
  },
  actions: {
    getUserInfo({ state },payload){
      console.log('payload',payload)
      api.getUserName(payload).then(({data})=>{
        // if(data.code === 1){
          state.userInfo = data.data
          // commit('setUserInfo',data)
        // }
      })
    },
    getAppRoutes({state},payload){
      console.log('state',state)
      api.getStaticHtml().then(({data})=>{
        if(data.code == 1){
          let routes = state.pages
          data.data.forEach(item=>{
            routes.push({
              path:`/${item.ad_code.replace('open-html-','')}`,
              name:`${item.ad_code.replace('open-html-','').toUpperCase()}`
            })
          })
          console.log(routes)
          // commit('setPages',routes)
          state.pages = routes
          // routes.forEach(router => {
          //   if (router.component) {
          //     // console.log('router.component',router)
          //     const component = router.component
          //     // console.log('-----','../src/views/' + router.name + '.vue')
          //     router.component = () => import(`../views/${router.name }.vue`)
          //     // router.component = resolve => {
          //       // 动态加载组件会编译加载项目所有组件
          //       // 这里不能全写变量，写开头确定起始地址，写结尾确定文件名
          //       // 这样就相当于编译'src/**/*.vue'，编译之后模块列表才会有所有的模块，传模块路径匹配才会命中
                
          //       // require([`../src/views/${router.name}.vue`], resolve)
          //     // }
          //   } else if (router.template) {
          //     router.component = resolve => {
          //       resolve({
          //         template: router.template
          //       })
          //     }
          //   } else {
          //     console.log('router',router)
          //     const component = `${router.name}/index`
          //     console.log('component',component)
          //     if(!router.name){
          //       console.log('router.redirect',router.redirect)
          //       router.component = () => import(`../views/forum/index.vue`)
          //     }else{
          //       console.log('router---',router.name)
          //       router.component = async resolve => {
          //         resolve({
          //           // template: `<div>${router.name}</div>`
          //           template: `<DetaultComponent name="${router.name}"/> `
          //         })
          //       }
          //     }
          //     // router.component = <DefaultTemplate/>
          //     // router.component = async resolve => {
          //       // 加载失败，不存在此模块，尝试加载本项目的视图
          //       // try {
          //       //   console.log('res---')
          //       //   // 尝试加载模块
          //       //   await require(['src/views/' + router.name + '.vue'], resolve)
          //       //   // router.component = () => import(`../views/${router.name }.vue`)
          //       // } catch {
          //       //   // 加载失败，不存在此模块，使用默认模板
          //       //   console.log(
          //       //     '(@|../..)/views/' + component + '.vue不存在，加载默认模板'
          //       //   )
          //         // resolve({
          //           // template: `<div>${router.name}</div>`
          //           // template: `<DetaultComponent name="${router.name}"/> `
          //         // })
          //       // }
          //     // }
          //   }
        
          //   let children = router.children
          //   if (children && children instanceof Array) {
          //     children = formatRoutes(children)
          //   }
        
          //   router.children = children
        
          //   fmRoutes.push(router)
        
          //   // addRouters.push(r)
          //   return fmRoutes
          // })
        }
      });
      
    }
  },
  modules: {
    course,
    inspiration,
    sucai,
    mine,
    search
  },
});
