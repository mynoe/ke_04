import api from '../../http/api/index'

let mine = {
  namespaced: true,
  state: {
    userSignStatus:null,
    learningCourse: [],
    sucai:[],
    linggan:[],
    mineInspiration:[],
    mineZanInspiration:[]
  },
  getters: {
    learningCourse: (state) => {
      return state.course
    },
    sucai:(state) =>{
      return state.cosucaiurse
    },
    linggan:(state) =>{
      return state.linggan
    },
    userSignStatus:(state)=>{
      return state.userSignStatus
    }
  },
  mutations: {
    setCourse(state, data) {
      state.course = data
    },
    setUserSignStatus(state,data){
      state.userSignStatus =data
    }
  },
  actions: {
    getMineCourse({state }, payload) {
      api.getMineCourse(payload).then(({ data }) => {
        // if(data.code === 1){
        state.learningCourse = data.data.data
        // }
      })
    },
    // 获取我的素材
    getMineSucai({state},payload){
      api.getMineSucai(payload).then(({data})=>{
        state.sucai = data.data
      })
    },
    // 灵感中心 我的灵感
    getMineInspiration({state},payload){
      api.getMineInspiration(payload).then(({data})=>{
        state.mineInspiration = data.data
      })
    },
    // 灵感中心 我点赞的灵感
    getMineZanInspiration({state},payload){
      api.getMineZanInspiration(payload).then(({data})=>{
        state.mineZanInspiration = data.data
      })
    },
    // 签到状态
    getUserSignStatus({commit,state}){
      console.log('----签到状态---')
      let params = {
        user_id:localStorage.getItem('userId')
      }
      api.getUserSignStatis(params).then(({data})=>{
        if(data.code === 1){
          console.log('----签到状态---',data)
          state.userSignStatus = data.data.sign_in
          commit('setUserSignStatus',data.data.sign_in)
        }
      })
    },
    
  },
}

export default mine
