import api from '../../http/api/index'

let course = {
  namespaced: true,
  state: {
    course:[],
    hotCourse:[],
    courseData:[],
    courseDetail:{},
    courseType:[],
    courseAd:{}
  },
  getters: {
    course: state => {
      return state.course
    },
    hotCourse:state =>{
      return state.hotCourse
    },
    courseList:state =>{
      return state.courseData
    }
  },
  mutations: {
    setCourse(state, data) {
      state.course = data
    }
  },
  actions: {
      getCourse({commit,state}){
        console.log(11111)
        api.getUsualCourse().then(({data})=>{
          // if(data.code === 1){
            state.course = data.data
            commit('setCourse',data.data)
          // }
        })
      },
      getHotCourse({ state }){
        api.getHotCourse().then(({data})=>{
          // if(data.code === 1){
            state.hotCourse = data.data

            // commit('setUserInfo',data)
          // }
        })
      },
      // 课程列表 带分页
      getCourseList({state},payload){
        api.getCourseList(payload).then(({data})=>{
          state.courseData = data.data
        })
      },
      // 课程详情
      getCourseDetail({state},payload){
        console.log('payload',payload)
        api.getCourseDetail(payload.id).then(({data})=>{
          state.courseDetail = data.data
        })
      },
      // 课程分类
      getCourseType({state}){
        api.getCourseType().then(({data})=>{
          let res = data.data
          res.unshift({type_name:'全部',id:''})
          state.courseType = res
        })
      },
      // 首页课程底部广告
      getCourseAd({state}){
        api.getCourseAd().then(({data})=>{
          state.courseAd = data.data
          var script = document.createElement('script');
          script.type="text/javascript";
          script.innerText = data.data[1].value
          document.body.appendChild(script);
        })
      }
  }
}

export default course