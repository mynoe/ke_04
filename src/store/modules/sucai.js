import api from '../../http/api/index'

export default {
  namespaced: true,
  state: {
    sucai5: [],
    sucai10: [],
    sucaiList: [],
    sucaiType:{},
    sucaiDetail:{}
  },
  getters: {
    sucai5: (state) => {
      return state.sucai5
    },
    sucai10: (state) => {
      return state.sucai10
    },
    sucaiList: (state) => {
      return state.sucaiList
    },
    sucaiType:(state) =>{
      return state.sucaiType
    }
  },
  mutations: {
    // setCourse(state, data) {
    //   state.course = data
    // }
  },
  actions: {
    getSucai5({ commit, state }) {
      api.getSucai5().then(({ data }) => {
        // if(data.code === 1){
        state.sucai5 = data.data
        commit('setCourse', data.data)
        // }
      })
    },
    getSucai10({ state }) {
      api.getSucai10().then(({ data }) => {
        // if(data.code === 1){
        state.sucai10 = data.data
        // commit('setUserInfo',data)
        // }
      })
    },
    // 素材页面 列表分页
    getSucaiList({state},payload) {
      api.getSucaiList(payload).then(({data})=>{
        state.sucaiList = data.data
      })
    },
    // 素材类别
    getSucaiType({state}){
      api.getSucaiType().then(({data})=>{
        state.sucaiType = data.data
      })
    },
    //素材详情
    getSucaiDetail({state},payload){
      api.getSucaiDetail(payload.id).then(({data})=>{
        if(data.code === 1){
          state.sucaiDetail = data.data
        }
        
      })
    }
  },
}
