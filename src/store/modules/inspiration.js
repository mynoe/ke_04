import api from '../../http/api/index'

export default {
  namespaced: true,
  state: {
    inspiration:[],
    inspirationHot:[],
    inspirationAd:{},
    inspirationMore:[],
    inspirationType:[]
  },
  getters: {
    inspiration: state => {
      return state.inspiration
    },
    hotInspiration:state =>{
      return state.inspirationHot
    }
  },
  mutations: {
    // setCourse(state, data) {
    //   state.course = data
    // }
  },
  actions: {
    getHotInspiration({state}){
        api.getHotInspiration().then(({data})=>{
          // if(data.code === 1){
            state.inspirationHot = data.data.data
            // commit('setCourse',data.data)
          // }
        })
      },
      getInspiration({ state }){
        api.getInspiration().then(({data})=>{
          // if(data.code === 1){
            state.inspiration = data.data.data

            // commit('setUserInfo',data)
          // }
        })
      },
      getInspirationAd({state}){
        api.getInspirationAd().then(({data})=>{
          state.inspirationAd = data.data
          var script = document.createElement('script');
          script.type="text/javascript";
          script.innerText = data.data[0].value
          document.body.appendChild(script);
        })
      },
      getInspirationMore({state},payload){
        api.getInspirationMore(payload).then(({data})=>{
          state.inspirationMore = data.data
        })
      },
      // 灵感类型
      getInspirationType({state}){
        api.getInspirationType().then(({data})=>{
          state.inspirationType = data.data
        })
      }
      
  }
}
