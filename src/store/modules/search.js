import api from '../../http/api/index'

let search = {
  namespaced: true,
  state: {
    searchList: [],
  },
  getters: {
    // searchList: (state) => {
    //   return state.searchList
    // },
  },
  mutations: {
    setSearchList(state, data) {
      state.searchList = data
    }
  },
  actions: {
    getSearchList({ commit,state },payload) {
      api.searchAction(payload).then(({data})=>{
        // if(data.code === 200){
          console.log('-----200----',data)
          console.log('-----state----',state)
          // state.searchList = data.data
          commit('setSearchList',data.data)
        // }
        
      })
    },
  },
}

export default search