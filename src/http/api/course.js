import axios from 'axios'
import qs from 'querystring'
let course = {
  /*
    获热门课程
  */
  getHotCourse(params) {
    return axios.get('/api/index/curriculum/get_hot_curriculum_list', params)
  },
  /*
    课程
  */
  getUsualCourse(params) {
    return axios.get('/api/index/curriculum/get_curriculum_list', params)
  },
  // 课程页面 列表分页
  getCourseList(params){
    let str = qs.stringify(params)
    console.log(str)
    return axios.get(`/api/index/curriculum/get_curriculum_list_all_page?${qs.stringify(params)}`)
  },
  // 课程详情
  getCourseDetail(curriculum_id){
    return axios.get(`/api/curriculum/show_curriculum_info?curriculum_id=${curriculum_id}`)
  },
  // 课程分类
  getCourseType(params){
    return axios.get(`/api/curriculum/show_curriculum_type`,params)
  },
  // 是否否关注
  getCourseFocusStatus(params){
    return axios.get(`/api/curriculum/is_add_collection_curriculums?${qs.stringify(params)}`)
  },
  // 关注课程
  addCourseFocus(params){
    return axios.get(`/api/curriculum/add_collection_curriculums?${qs.stringify(params)}`)
  },
  // 取关
  removeCourseFocuse(params){
    return axios.get(`/api/curriculum/cancel_collection_curriculums?${qs.stringify(params)}`)
  },
  // 课程底部广告
  getCourseAd(){
    return axios.get(`/api/index/curriculum/get_bottom_ad`)
  },


  // 3张灵感
  getHotInspiration(params) {
    return axios.get('/api/index/inspiration/get_hot_inspiration', params)
  },
  // 普通灵感
  getInspiration(params) {
    return axios.get('/api/index/inspiration/get_inspiration', params)
  },
  // 普通灵感底部广告
  getInspirationAd(){
    return axios.get(`/api/index/inspiration/inspiration_bottom_ad`)
  },
  // 普通灵感详情页底部广告
  getInspirationDetailAd(){
    return axios.get(`/api/index/inspiration/inspiration_info_bottom_ad`)
  },
  // 灵感更多
  getInspirationMore(params) {
    return axios.get(`/api/index/inspiration/get_inspiration_list_all_page?${qs.stringify(params)}`, )
  },
  // 灵感中心 我的灵感
  getMineInspiration(params){
    return axios.get(`/api/inspiration/get_me_inspiration_page?${qs.stringify(params)}`)
  },
  // 灵感中心 我点赞的灵感
  getMineZanInspiration(params){
    return axios.get(`/api/inspiration/user_link_inspiration/get_me_inspiration_page?${qs.stringify(params)}`)
  },
  // 灵感类型
  getInspirationType(){
    return axios.get(`/api/inspiration/inspiration/get_inspiration_type`)
  },
  // 发布灵感
  publishInspiration(params){
    return axios.post(`/api/inspiration/inspiration/add_inspiration`,params)
  },
  // 收藏 点赞灵感
  zanInspiration(params){
    return axios.get(`api/inspiration/user_link_inspiration/user_link_inspiration?${qs.stringify(params)}`)
  },
  // 灵感详情
  getInspirationDetail(params){
    return axios.get(`/api/inspiration/inspiration/get_inspiration_info?${qs.stringify(params)}`)
  }
}
export default course
