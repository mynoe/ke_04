import axios from 'axios'
import qs from 'querystring'
let forum = {
  // 发帖
  /**
   * 
   * @param {string} title 
   * @param {string} content 
   * @param {number} forum_type_id 
   * @param {number} user_id 
   */
  pubForum(params){
    return axios.post(`/api/forum/forum/add_forum`,params)
  },
  // 帖子列表 page
  getForumList(params){
    return axios.get(`/api/forum/forum/get_forum?${qs.stringify(params)}`)
  },
  // 帖子详情
  getForumDetail(params){
    return axios.get(`/api/forum/forum_follow/get_forum_info?${qs.stringify(params)}`)
  },
  // 帖子是否收藏
  getForumStatus(params){
    return axios.get(`/api/forum/forum_follow/is_forum_follow_card?${qs.stringify(params)}`)
  },
  // 获取帖子评论
  getForumComments(params){
    return axios.get(`/api/forum/forum_type/get_forum_type?${qs.stringify(params)}`)
  },
  /**
   * 发布帖子评论 
   * forum_id
   * content 
   */
  pubForumConnent(params){
    return axios.post(`/api/forum/forum_comment/add_forum_comment`,params)
  },
  /**
   * 获取帖子类型
   * forum_id
   */
  get_forumType(params){
    return axios.get(`/api/forum/forum_type/get_forum_type?${qs.stringify(params)}`)
  },
  // 获取自己发布的帖子
  getPublishedForum(params){
    return axios.get(`/api/forum/forum/get_self_forum?${qs.stringify(params)}`)
  },
  // 回复的
  getReplyedForum(params){
    return axios.get(`/api/forum/forum_comment/get_self_forum_comment?${qs.stringify(params)}`)
  },
  // 添加关注
  addToCollect(params){
    return axios.post(`/api/forum/forum_follow/add_forum_follow_card`,params)
  },
  // 关注的
  getFocusedForum(params){
    return axios.get(`/api/forum/forum_follow/get_forum_follow_card?${qs.stringify(params)}`)
  },
  // 关注的版块
  getFocusedBlock(params){
    return axios.get(`/api/forum/forum_follow/get_forum_follow_section?${qs.stringify(params)}`)
  }
}
export default forum
