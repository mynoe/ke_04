import axios from 'axios'
import qs from 'querystring'
let user = {
  /**
   * name
   * password
   * */

  login(params) {
    return axios.post('/api/user/login', params)
  },
  logout() {
    return new Promise((resolve,reject)=>{
      const data = {
        data:{
          code:1,
          msg:'退出登陆'
        }
      }
      resolve(data)
    })
  },
  /*注册
   *@params
   *name [string]
   *mobile [string]
   *email [string]
   *password [string]
   * */

  regist(params) {
    return axios.post('api/user/reg', params)
  },
  /*修改email
   *@params
   *email [string]
   * */

  updateEmail(params) {
    return axios.post('api/user/update_email', params)
  },
  /*修改密码
   *@params
   *password [string]
   * */

  updatePassword(params) {
    return axios.post('api/user/update_password', params)
  },
  /*修改手机号
   *@params
   *mobile [string]
   * */

  updateMobile(params) {
    return axios.post('api/user/update_mobile', params)
  },
  /*修改昵称
   *@params
   *name [string]
   * */

  updateNickname(params) {
    return axios.post('api/user/update_name', params)
  },
  /*获取用户信息
   *@params
   *mobile [string]
   * */

  getUserName(params) {
    return axios.get(`/api/user/getuserinfo?${qs.stringify(params)}`)
  },
  /**
   * 全量更新用户信息
   */
  updateUserInfo(params){
    return axios.post(`/api/user/update_user_details`,params)
  },
  // 获取用户全量信息
  getUserAllInfo(params){
    return axios.post(`/api/user/getuserinfo`,params)
  },
  /**
   * 获取资料完善程度
   */
  getUserInfoPercent(params){
    return axios.get(`/api/user/get_data_perfection?${qs.stringify(params)}`)
  },
  /**
   * 获取用户任务
   * @param {*} params 
   */
  getUserMission(params){
    return axios.get(`/api/task/task/get_self_task?${qs.stringify(params)}`)
  },
  /**
    获取关注，粉丝，被赞，帖子
   */
  getUserPpp(params){
    return axios.get(`/api/user/get_user_ppp?${qs.stringify(params)}`)
  },
  /**
   * 个人动态
   */
  getUserActions(params){
    return axios.get(`/api/user/user_trend?${qs.stringify(params)}`)
  },
  // 签到状态
  getUserSignStatis(params){
    return axios.get(`/api/user/get_sign_in?${qs.stringify(params)}`)
  },
  // 签到
  signIn(params){
    return axios.get(`/api/user/add_sign_in?${qs.stringify(params)}`)
  },
  // 更新头像
  updateAvator(params){
    return axios.post(`/api/user/updata_head_sculpture`,params)
  },
  //手机注册获取验证码
  getMobileCode(params){
    return axios.get(`/api/code/get_mobile_code?${qs.stringify(params)}`,)
  },
  // 密码重置 发送短信验证码
  resetPassPhone(params){
    return axios.post(`/api/user/verify_mobile`,params)
  },
  // 邮箱注册获取验证码
  getEmailCode(params){
    return axios.get(`/api/code/get_email_code?${qs.stringify(params)}`,)
  },
  // 密码重置 通过邮箱
  resetPassEmail(params){
    return axios.post(`/api/user/verify_email`,params)
  },  
  // 密码重置
  passReset(params){
    return axios.post(`/api/user/set_user_password_by_mobile_or_email`,params)
  },

  // /api/ad/ad/get_ad_tiny 首页幻灯相关接口
  getMediaList(params){
    return axios.get(`/api/ad/ad/get_ad_tiny?${qs.stringify(params)}`)
  },
  // 专题页接口/api/ad/ad/get_ad_special
  getSpecial(params){
    return axios.get(`/api/ad/ad/get_ad_special?${qs.stringify(params)}`)
  },
  // 广告位置
  getSideAd(id){
    return axios.get(`/api/ad/ad/get_ad_position_id?position_id=${id}`)
  },
  // 课程详情广告
  getCourseDetailAd(){
    return axios.get(`api/ad/ad/get_ad_info`)
  },
  // 根据内容id获取广告
  getContentById(id){
    return axios.get(`/api/ad/ad/get_ad_content_id?content_id=${id}`)
  },
  // 获取staticHtml  api/ad/ad/get_ad_type
  getStaticHtml(){
    return axios.get(`/api/ad/ad/get_ad_type`)
  },
  getCodeContent(code){
    return axios.get(`/api/ad/ad/get_ad_code?code=${code}`)
  }
}
export default user
