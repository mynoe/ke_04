import axios from 'axios'
import qs from 'querystring'
let mine = {
  // 学习的课程
  getMineCourse(params){
    return axios.get(`/api/curriculum/get_me_curriculum?user_id=${params.user_id}&page=${params.page}`)
  },
  // 参加课程
  joinCourse(params){
    return axios.post(`/api/curriculum/add_me_curriculum`,params)
  },
  //
  beforJoinCourse(params){
    // /api/curriculum/is_add_curriculum?curriculum_id={curriculum_id}&user_id={user_id}
    return axios.get(`/api/curriculum/is_add_curriculum?${qs.stringify(params)}`)
  },
  // 我的素材
  getMineSucai(params){
    return axios.get(`/api/material/material/get_me_material_page?${qs.stringify(params)}`)
  },
  // 发表素材
  sendSucai(params){
    return axios.post(`/api/material/material/add_material`,params)
  }

}
export default mine
