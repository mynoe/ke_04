import axios from 'axios'
import qs from 'querystring'
let active = {
  // 参加活动
  joinActives(params){
    return axios.post(`/api/activity/activity/participate_in_activities`,params)
  },
  // 活动详情
  activeDetail(params){
    return axios.get(`/api/activity/activity/show_activities_info?${qs.stringify(params)}`)
  },
  // 活动列表
  getActiveList(params){
    return axios.get(`/api/ad/show_activity_list?${qs.stringify(params)}`)
  },
  // 活动热门推荐
  getHotActives(params){
    return axios.get(`/api/ad/show_activity_hot_recommend?${qs.stringify(params)}`)
  },
  // 活动页面
  getHomePageActive(params){
    return axios.get(`/api/index/ad/show_activity?${qs.stringify(params)}`)
  }
}
export default active
