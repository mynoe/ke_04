import axios from 'axios'
import qs from 'querystring'
let sucai = {
  /*
    5个素材分类
  */
  getSucai5(params) {
    return axios.get('/api/index/material/get_hot_material', params)
  },
  /*
    10个素材图片
  */
  getSucai10(params) {
    return axios.get('/api/index/material/get_material', params)
  },
  // 底部广告
  getSucaiBottomAd(params){
    return axios.get(`/api/index/material/material_bottom_ad?${qs.stringify(params)}`)
  },
  // 素材页面 列表分页
  getSucaiList(params){
    return axios.get(`/api/material/get_material_list_all_page?${qs.stringify(params)}`)
  },
  // 素材类别
  getSucaiType(params){
    return axios.get(`/api/material/material/get_type_and_format_and_pattern`,params)
  },
  getSucaiDetail(id){
    return axios.get(`/api/material/material/show_material_info?id=${id}`)
  },
  // 收藏素材 user_id material_id
  collectSucai(params){
    return axios.get(`api/material/material/user_link_material?${qs.stringify(params)}`)
  },
  // 取消收藏
  cancleCollectSucai(params){
    return axios.get(`/api/material/material/cancel_link_material?${qs.stringify(params)}`)
  },
  // 收藏素材状态 
  getCollectSucaiStatus(params){
    return axios.get(`api/material/material/is_user_link_material?${qs.stringify(params)}`)
  },
  // 
  /**
   * 素材中心分类
   *  user_id
   *  type
   *  order
   **/ 
  getCenterSucaiList(params){
    return axios.get(`api/material/user_link_material/get_user_link_material?${qs.stringify(params)}`)
  },
  // 下载素材
  downLoadSucai(params){
    return axios.get(`/api/material/material/get_material_file?${qs.stringify(params)}`)
  }

}

export default sucai
