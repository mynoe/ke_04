import axios from 'axios'
import '../config'
import user from './user'
import course from './course'
import sucai from './sucai'
import mine from './mine'
import active from './active'
import forum from './forum'

import qs from 'querystring'
let api = {
    ...user,
    ...course,
    ...sucai,
    ...mine,
    ...active,
    ...forum,
  
  // 获取轮播信息
  getAdvertisementList() {
    return axios.get('/api/index/ad/get_head_round_advertisement_list')
  },
  // 获取轮播数据
  getHotRec(params) {
    return axios.get('/api/index/ad/get_recommend_list', params)
  },
  // 友情链接
  getFriendLink(params){
    return axios.get('/api/index/get_friendship_links', params)
  },
  // 导航栏
  getNavInfo(params){
    return axios.get('/api/index/get_navigation_bar', params)
  },
  // 搜索
  searchAction(params){
    return axios.get(`/api/index/search_books?${qs.stringify(params)}`)
  },
  // 上传图片
  upLoadImg(params){
    return axios.post(`/ueditor1_4_3_3/utf8-php/php/controller.php?action=uploadimage`,params)
  },
  // 获取首页模板
  getHomeTemplage(){
    return axios.get(`/api/index/page_style`)
  }
}
export default api
